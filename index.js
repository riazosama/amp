const express = require('express');
const app = express();
app.use(express.static('public'));

app.set('view engine', 'pug')

app.get("/", (_, response) => {  
  response.render('index');
});

app.get("/page2", (_, response) => {  
  response.render('page2');
});

app.get("/page3", (_, response) => {  
  response.render('page3');
});

app.get("/page4", (_, response) => {  
  response.render('page4');
});

app.get("/page5", (_, response) => {  
  response.render('page5');
});

let listener = app.listen(8181, () => {
  console.log('Your app is listening on port ' + listener.address().port);
});